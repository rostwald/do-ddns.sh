do_ddns
=======

Simple script to add/update DNS record at DigitalOcean via API.  
Apart from standard tools it depends on curl, which is usually not part of the standard installation.  

The Script takes these variables as command-line arguments, which can be set to a default value within the script itself:  

 \-h \<HOST\> The Hostname that should be managed, will be extracted from 'hostname' if unset  
 \-i \<IP\> IP that should go into the Record, will be taken from the interface that connects to the default gateway if unset  
 \-6 \<IPv6 prefix\> Optional IPv6 prefix if the script should set an AAAA record (no auto-detection)  
 \-k \<API_KEY\> The DO API-Key, needs to be generated with read/write permissions. Required for the Script to actually run.  
 \-t \<TTL\> record TTL
 \-z \<ZONE\> The Zone in which the Record should be placed, will be extracted from 'hostname' if unset  


###### Limitations:  
- This script can't deal with multiple A-Records for a host and will only update the first RECORD_ID that is being returned from the API.
- The script always tests/sets an A-Record; AAAA-Records are optional. It can't work only for AAAA-Records.
- Reading the Values for HOST and ZONE with 'hostname' *only* works for BSDs and other OS that give the FQDN when 'hostname' is invoked without additional arguments.
- Determining the IP automatically relies on the tried-and-proven standard tools 'route' and 'ifconfig' which are present on all UNIX. Linux either removed them completely or uses ~~broken~~ ~~weird~~ 'special' variants of those tools, so auto-detection will most likely __NOT__ work on Linux.
