#!/bin/sh
API_URL="https://api.digitalocean.com/v2/domains/"
API_KEY=""
HOST=""
IP=""
IP6=""
TTL=""
ZONE=""

usage() {
	echo $(basename "$0"): ERROR: "$@" 1>&2
	echo "usage: $(basename "$0") '[-h <host>] [-i <ip>] [-6 <ipv6>] [-k <api-key>] [-t <ttl>] [-z <zone>]'

	All variables can be set to a default value inside the script.
	If Hostname and Zone are not set, they will be guessed from the systems hostname
	If IP is not set, the first IP of the default route interface is used"
	exit 1
}

generate_payload() {
	cat <<EOF
{
 "type": "A",
 "name": "$HOST",
 "ttl": "$TTL",
 "data": "$IP"
}
EOF
}

generate_payload6() {
	cat <<EOF
{
 "type": "AAAA",
 "name": "$HOST",
 "ttl": "$TTL",
 "data": "$IP"
}
EOF
}

while :
do
	case "$1" in
		-h) shift; HOST=$1;;
		-i) shift; IP=$1;;
		-6) shift; IP6=$1;;
		-k) shift; API_KEY=$1;;
		-t) shift; TTL=$1;;
		-z) shift; ZONE=$1;;
		--) shift; break;;
		-*) usage "bad argument $1";;
		*) break
	esac
	shift
done

## We can't do anything useful without an API_KEY, so we fail early if not set
[ "$API_KEY" == "" ] && echo "API KEY required" && exit 1 

## get variables from the system if they are not set

# set hostname and zone; this only works with FreeBSD and OpenBSD which know and give their FQDN (given it was properly set)
[ "$HOST" == "" ] && HOST="$(hostname | cut -d'.' -f1)"
[ "$ZONE" == "" ] && ZONE="$(hostname | cut -d'.' -f2-)"
# get IP from the interface which connects to the default gateway
[ "$IP" == "" ] && IP="$( ifconfig `netstat -nr | awk '/default/ { print $NF }'` | awk '/inet/ { print $2 }' )"


### now we actually talk to DO

## try to get a record ID, this way we see if there is already a record for this host
RECORD_ID=$(curl -sX GET -H "Content-Type: application/json" \
	-H "Authorization: Bearer $API_KEY" \
	"$API_URL/$ZONE/records"  | tr "{" "\n" | grep $HOST | grep '"type":"A"' | tr "," "\n" | grep '"id":' | cut -d':' -f2 )

RECORD_ID6=$(curl -sX GET -H "Content-Type: application/json" \
	-H "Authorization: Bearer $API_KEY" \
	"$API_URL/$ZONE/records"  | tr "{" "\n" | grep $HOST | grep '"type":"AAAA"' | tr "," "\n" | grep '"id":' | cut -d':' -f2 )

## check if we have a RECORD_ID and if it is numeric, which means there is already a record we need to update
if [ $RECORD_ID -gt 0 2> /dev/null ]; then
	##update record
	curl -sX PUT -H "Content-Type: application/json" \
		-H "Authorization: Bearer $API_KEY" \
		-d "$(generate_payload)" \
		"$API_URL/$ZONE/records/$RECORD_ID" | grep "domain_record" >/dev/null || echo  "A record update failed"
else
	## create new record
	curl -sX POST -H "Content-Type: application/json" \
		-H "Authorization: Bearer $API_KEY" \
		-d "$(generate_payload)" \
		"$API_URL/$ZONE/records" | grep "domain_record" >/dev/null || echo "A record creation failed"
fi

## same for IPv6 if IP6 is set
if [ "$IP6" != "" ]; then
	if [ $RECORD_ID6 -gt 0 2> /dev/null ]; then
		##update record
		curl -sX PUT -H "Content-Type: application/json" \
			-H "Authorization: Bearer $API_KEY" \
			-d "$(generate_payload6)" \
			"$API_URL/$ZONE/records/$RECORD_ID" | grep "domain_record" >/dev/null || echo "AAAA record update failed"
	else
		## create new record
		curl -sX POST -H "Content-Type: application/json" \
			-H "Authorization: Bearer $API_KEY" \
			-d "$(generate_payload6)" \
			"$API_URL/$ZONE/records" | grep "domain_record" >/dev/null || echo "AAAA record creation failed"
	fi
fi
